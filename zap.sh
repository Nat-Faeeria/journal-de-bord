#!/bin/bash
mkdir reports/ # cree le dossier auquel Gitlab-CI 
               # pourra acceder pour generer les artifacts

mkdir /zap/wrk # dossier obligatoire quand on utilise zap dans Docker
               # c'est dans ce dossier que sont ecrits les rapports

# Appel du script full-scan de ZAP sur la cible $TARGET, en demandant
# de generer un rapport html, les logs de debug et en limitant (en theorie)
# l'analyse a 5 minutes
/zap/zap-full-scan.py -t $TARGET -r report.html -d -m 5

cp /zap/wrk/* reports/  # recuperation des rapports

# envoi d'une valeur de retour positive ou negative selon si 
# le scan a fonctionne ou non
returnCode=0
if grep -q "<!DOCTYPE html>" reports/report.html ; then
  echo "ok"
  returnCode=1
fi
echo $returnCode